CREATE DATABASE `baseuno` CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;


CREATE DATABASE `basedos` CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

USE `baseuno`;

CREATE DEFINER=`carlos`@`localhost` PROCEDURE `baseuno`.`insertar_registros`()
BEGIN
	
    DECLARE finaliza INT DEFAULT 0;
	DECLARE tempnombre  varchar(20);
	DECLARE tempcedula INT(12);
	DECLARE tempid INT;
	DECLARE cursor1 CURSOR FOR	SELECT * FROM tabla1 WHERE id <= 50;

    DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET finaliza = 1;


	
	-- Crea tabla1
	DROP TABLE IF EXISTS baseuno.tabla1;
	CREATE TABLE baseuno.tabla1 (
		id INT auto_increment NULL,
		nombre varchar(20) NULL,
		cedula INT(12) DEFAULT 0 NULL,
		CONSTRAINT tabla1_PK PRIMARY KEY (id)
	)
	ENGINE=InnoDB
	DEFAULT CHARSET=utf8mb4
	COLLATE=utf8mb4_unicode_ci;

	-- Crea tabla2
	
	DROP TABLE IF EXISTS baseuno.tabla2;
	CREATE TABLE baseuno.tabla2 (
		id INT auto_increment NULL,
		nombre varchar(20) NULL,
		cedula INT(12) DEFAULT 0 NULL,
		CONSTRAINT tabla2_PK PRIMARY KEY (id)
	)
	ENGINE=InnoDB
	DEFAULT CHARSET=utf8mb4
	COLLATE=utf8mb4_unicode_ci;


	-- Inserta registros

	INSERT INTO baseuno.tabla1
		(nombre, cedula) VALUES
		('Demócrito Medina',DEFAULT),
		('Dámaso Gutiérrez',563049),
		('Enrique Román',824577),
		('Roberto Sanz',395278),
		('Erico Suárez',211972),
		('Tadeo Guerrero',312525),
		('Aristides Gallego',636912),
		('Yolanda Parra',91994),
		('Remedios Peña',496894),
		('Francesc Delgado',589597),
		('Leopoldo Sanz',175302),
		('Lorena Ramos',DEFAULT),
		('Andrés Vázquez',278620),
		('Hugo Márquez',821492),
		('Jacobo Esteban',503226),
		('Mohamed Romero',222426),
		('Dámaso Pascual',329075),
		('Jordi Garrido',106701),
		('Josep Caballero',988164),
		('Milagros Santiago',937184),
		('Félix Parra',315597),
		('Alfonso Blesa',599320),
		('Claudio Garrido',722499),
		('Odón Soto',15845),
		('Josefina Reyes',131023),
		('Carina Caballero',287395),
		('Victoria Rey',997056),
		('Lorenzo Torres',176100),
		('Celso Cano',907165),
		('Francisco Alonso',257156),
		('Mateo Crespo',516621),
		('Feliciano Montoro',266830),
		('Fátima Lozano',531429),
		('Reinaldo Crespo',131404),
		('Patricio Martínez',615573),
		('Félix Cortés',675993),
		('Genoveva Santana',149358),
		('Severino Caballero',770539),
		('Adalberto Méndez',389764),
		('Rocío Herrera',219471),
		('Josafat Rubio',295556),
		('César Alonso',331986),
		('Cirino Velasco',581368),
		('Aurelio Blesa',922090),
		('Rubén García',516377),
		('Eduardo Durán',592114),
		('Benedicto Díaz',835264),
		('Cleofás Fuentes',175372),
		('Poncio Cruz',458742),
		('Oscar Campos',709831),
		('Víctor Herrero',247313),
		('Ángeles Ibáñez',227511),
		('Laureano Blanco',633775),
		('Sonia Álvarez',357879),
		('Sara Gil',302776),
		('Isabel Reyes',991887),
		('Lázaro Rey',657806),
		('Gregorio Martín',942936),
		('Feliciano Moya',628213),
		('Jacob Cambil',DEFAULT),
		('Vanesa Pastor',743140),
		('David Méndez',772321),
		('Francisco Alonso',426866),
		('Fidel Herrera',573630),
		('Joan Gallego',710811),
		('José Reyes',1341),
		('Inocencio Herrero',77620),
		('Gonzalo Ortega',883965),
		('Noé Montoro',176806),
		('Antonio Herrero',508350),
		('Salomé Cortés',335885),
		('Ireneo Hernández',994918),
		('Celina Gutiérrez',363802),
		('Basileo Lorenzo',385073),
		('Samuel Lorenzo',811635),
		('Carolina Calvo',976166),
		('Mohamed Vázquez',891646),
		('Nuria Ruiz',479288),
		('Ángela Montoro',527642),
		('Concepción Moya',981027),
		('Benito Moreno',296699),
		('Porfirio Herrero',667040),
		('Oseas Hidalgo',260332),
		('Fabiola Navarro',723162),
		('Ciro Campos',474103),
		('Roberto Suárez',538101),
		('Nemesio Ramírez',888112),
		('Tadeo Cambil',130034),
		('Celina Castro',182890),
		('Ifigenia Caballero',180090),
		('Gerardo Fuentes',644555),
		('Inocencio Hernández',306356),
		('Priscila Ortiz',174196),
		('Sixto Gallardo',81813),
		('Olga Navarro',596275),
		('Edmundo Díaz',165411),
		('Aristides Marín',325928),
		('Valeriano Prieto',195621),
		('Ubaldo Medina',978691),
		('Celina Hidalgo',DEFAULT);
	
	
	
		-- Inserta registros
	
		INSERT INTO baseuno.tabla2
		(nombre, cedula) VALUES
		('Elena Giménez',683285),
		('Roque Ferrer',441690),
		('Míriam Rey',946846),
		('Efrén Cruz',241019),
		('Urbano Vicente',706919),
		('Guzmán Jiménez',728643),
		('Mateo Díez',512562),
		('Olga Gutiérrez',979633),
		('Sara Martínez',614213),
		('Carolina Carmona',428393),
		('Víctor Márquez',873758),
		('Magdalena Ruiz',399565),
		('Celina Santiago',872009),
		('Cirino Vázquez',828624),
		('Juan Pastor',954015),
		('Oto Cambil',424709),
		('Ester Díaz',783346),
		('Luisa Ortega',DEFAULT),
		('Xavier Cano',253503),
		('Dolores Méndez',89652),
		('Sandra Flores',5139),
		('Casiano Soler',246984),
		('Isidro Rodríguez',781787),
		('Ana Mora',459852),
		('Albina Montero',170705),
		('Rosendo Aguilar',8275),
		('Jaume Ruiz',187088),
		('Arturo Durán',862090),
		('Victoria Álvarez',442360),
		('Leopoldo Márquez',869823),
		('Justino Santana',52088),
		('Casio Cambil',798382),
		('Velerio Santana',828028),
		('Juan Sáez',935420),
		('Bárbara Gil',721041),
		('Héctor González',479145),
		('Demócrito Calvo',707115),
		('Leopoldo Pérez',136311),
		('Victoria Vidal',747339),
		('Julián Navarro',593299),
		('Paula Domínguez',880702),
		('Sansón Santana',98652),
		('Aurora Garrido',449913),
		('Esteban Sánchez',955183),
		('Benito Montoro',850308),
		('Fabiola Sanz',654107),
		('Otilia García',381683),
		('Gisela Castillo',444899),
		('Leocadia Santana',971422),
		('Natalia Gil',45694),
		('Josefa Carrasco',229396),
		('Elías Calvo',353378),
		('Ángeles Muñoz',37314),
		('Roque Iglesias',735697),
		('Marc Romero',11527),
		('Adolfo Soler',58042),
		('Ananías Cruz',733167),
		('Leandro Soler',269420),
		('Ildefonso Rey',975565),
		('Gregorio Aguilar',648949),
		('Zaqueo Morales',911505),
		('Abrahán Vicente',DEFAULT),
		('José Pastor',534653),
		('Bernardo Cabrera',394452),
		('Alejandra Domínguez',345524),
		('Albina Hernández',442219),
		('César Bravo',603848),
		('Octavio Castillo',925734),
		('Humberto Román',245733),
		('Fabiola Moreno',472844),
		('Edgar Mora',935896),
		('Conrado Rey',93749),
		('Vidal Moreno',713203),
		('Sergio Díez',232538),
		('Ascensión Nieto',761618),
		('Alonso Flores',586821),
		('Verónica Benítez',712687),
		('Federico Moya',656393),
		('Casiano Garrido',76404),
		('Miguel Sanz',397473),
		('Gustavo Montero',305880),
		('Marcelo Durán',902502),
		('Velerio Durán',837219),
		('Benedicto Hidalgo',830419),
		('Lidia Fernández',311997),
		('Esteban Torres',25591),
		('José Márquez',778181),
		('Cristina Gallardo',61773),
		('Donato Montero',331870),
		('Antonio Vega',413856),
		('Mar González',542550),
		('Vladimiro Cortés',434967),
		('Bernardo Martín',376990),
		('Inés Ramos',228020),
		('Erico Fuentes',63624),
		('Valentín Vargas',118449),
		('Aquiles Alonso',758064),
		('Ireneo Ibáñez',820013),
		('Rosalia Durán',12334),
		('Andrea Gallego',247338);


	-- Uso de cursor -> insertar en tabla2
	
	OPEN cursor1;
		 
		insertando :LOOP 
			FETCH cursor1 into tempid,tempnombre,tempcedula;
			
			IF (finaliza = 1) THEN
	            LEAVE insertando;
	        END IF;

			INSERT INTO baseuno.tabla2
			(nombre, cedula) VALUES (tempnombre,tempcedula);
		
		
			IF (tempid<=20) THEN
			
				DELETE FROM baseuno.tabla1 WHERE id = tempid;
			
			END IF;
			
			
		END LOOP insertando;
	
	CLOSE cursor1;

	
END;

