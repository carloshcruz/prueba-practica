<?php
     $servidor = "localhost";
     $usuario = "carlos";
     $contrasena = "carlos";
     $tabla = "baseuno";
 
      $conn = mysqli_connect($servidor, $usuario, $contrasena);
      mysqli_select_db($conn, $tabla);

      $nombretabla = $_GET['nombretabla']
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Prueba</title>

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    </head>
    <body>
        <nav class="navbar navbar-dark bg-primary">
            <a class="navbar-brand" href="/">Navbar</a>
        </nav>
        <div class="container">
            <div class="mt-5">
                <form class="form">
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Seleccione Tabla</label>
                        <select name="nombretabla" class="form-control" id="exampleFormControlSelect1">
                        <option value=""></option>
                        <option value="tabla1">Tabla 1</option>
                        <option value="tabla2">Tabla 2</option>
                        </select>

                    </div>
                    <button type="submit" class="btn btn-primary mb-2">Consultar</button>
                </form>
            </div>
            
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col" colspan="3">Registros de : <?php echo $nombretabla; ?></th>
                    </tr>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Cedula</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                       
                       $num = 0;
                       if ($nombretabla != null) {
                          
                            $sql = "SELECT * FROM $nombretabla";
                            $result = $conn->query($sql);

                            while($registro = mysqli_fetch_assoc($result)){

                               echo ' 
                                   <tr>
                                       <th scope="row">'.++$num.'</th>
                                       <td>'.$registro['nombre'].'</td>
                                       <td>'.$registro['cedula'].'</td>
                                   </tr>
                               ';

                            }


                       }

                   ?>
                </tbody>
            </table>
                    

        
        </div>

    </body>
</html>
